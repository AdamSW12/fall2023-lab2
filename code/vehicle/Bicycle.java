/* Adam Winter
 * 2242433
 */
package vehicle;
public class Bicycle{
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;
    
    public Bicycle(String manufacturer, int numberGears, double maxSpeed) {
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }

    public int getNumberGears() {
        return numberGears;
    }

    public double getMaxSpeed() {
        return maxSpeed;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    @Override
    public String toString(){
        return "Manufacturer: " + this.manufacturer + ", Gears: " + this.numberGears + ", Max speed: " + this.maxSpeed;
    }
    
}