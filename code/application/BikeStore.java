package application;
import vehicle.Bicycle;
public class BikeStore {
    public static void main(String[] args) {
        Bicycle[] bicycles = new Bicycle[4];

        bicycles[0] = new Bicycle("Luffy", 5, 50);
        bicycles[1] = new Bicycle("David & Co.", 3, 20);
        bicycles[2] = new Bicycle("CheeseMaestro", 6, 30);
        bicycles[3] = new Bicycle("Yamaha", 7, 40);

        for (Bicycle bicycle : bicycles) {
            System.out.println(bicycle);
        }
    }
}
